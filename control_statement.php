<?php
//if else
$num1=10;
$num2=5;

if($num1>$num2){
    echo $num1." is greater than ".$num2."<br>";
}
else{
    echo $num2." is greater than ".$num1."<br>";
}

//switch statement
$students_num=80;
switch($students_num){
    case '80':
        echo "grade is A";
        break;
    case 70:
        echo "grade is B";
        break;
    default:
        echo "grade is F";
}
echo "<br>";

//print even number
$var=1;
while($var!=11) {
    if ($var % 2 == 0) {
        echo $var . " ";
    }
    $var++;
}
//print 1 to 10
echo "<br>";
for($i=1;$i<=10;$i++){
    echo $i." ";

}
echo"<br>";
$j=1;
while($j<11){
    echo $j." ";
    $j++;
}
echo "<br>";

//do while loop

$number=10;
do {
    echo "Executed 1 times from do while loop";
    $number++;
}
while($number>14);

echo "<br>";

//for each loop

$cars = array("bmw","toyota","ferarri","marcedes");

foreach($cars as $key=>$value){
    echo "$key ";
    echo "$value <br>";
}